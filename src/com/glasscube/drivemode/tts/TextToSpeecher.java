package com.glasscube.drivemode.tts;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.ContactsContract.PhoneLookup;
import android.speech.tts.TextToSpeech;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.glasscube.drivemode.audiomanager.DAudioManager;
import com.glasscube.drivemode.audiomanager.DSoundpool;
import com.glasscube.drivemode.broadcasthandler.PhoneStateBroadcastReceiver;
import com.glasscube.drivemode.voicerecognition.VoiceRecognitionActivity;

public class TextToSpeecher extends Service implements TextToSpeech.OnInitListener,TextToSpeech.OnUtteranceCompletedListener{

    private TextToSpeech mTts;
    private String phonenumber;
    private AudioManager audiomanager;
    private boolean  voicerecenabled = false , phonestatechanged = false;
    private int stream = 0;
    
    @Override
	public void onCreate()
    {
    	super.onCreate();
    	audiomanager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
    	this.phonestatechanged = false;
    	addTelephoneStatelistner();
    	
    	if (DAudioManager.headsetplugged)
    	{
			stream = AudioManager.STREAM_RING;
			setTtsVolume(5);
    	}
		else
		{
			stream = AudioManager.STREAM_MUSIC;
    		setTtsVolume(15,4);
		}	
    	mTts = new TextToSpeech(this,this);    
    }
    
    private void setTtsVolume(int rlevel)
    {
     	audiomanager.setStreamVolume(AudioManager.STREAM_RING,rlevel,0);
    }
  
    private  void setTtsVolume(int mlevel,int rlevel)
	{
      	audiomanager.setStreamVolume(AudioManager.STREAM_MUSIC,mlevel,0);
		audiomanager.setStreamVolume(AudioManager.STREAM_RING,rlevel,0);
	}
    
	protected void speechText(String text)
    {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,"stringId");		
		params.put(TextToSpeech.Engine.KEY_PARAM_STREAM,String.valueOf(stream));
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		boolean ttsenabled = preferences.getBoolean("tts_enable", true);
		voicerecenabled = preferences.getBoolean("voicerec_enable", true);
				
		if (ttsenabled)
		{
			if (!text.equals("unknown"))
	    		mTts.speak(text+" is calling do you want to answer",TextToSpeech.QUEUE_FLUSH,params);
	    	else
	    		mTts.speak("call from unknown number do you want to answer",TextToSpeech.QUEUE_FLUSH,params);
		}
		else if (voicerecenabled)
		{
			startVoicerec();
		}
		else
			stopSelf();
    }
    
	private String getContactName(String sPhonenumber)
    {
    	Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,sPhonenumber);
        Cursor people = getContentResolver().query(uri, new String[] {PhoneLookup.DISPLAY_NAME},null,null, null);
        String name = null;
        if (people.moveToFirst())
        {
        	int nameIndex = people.getColumnIndex(PhoneLookup.DISPLAY_NAME);
        	name = people.getString(nameIndex);
        }
        else 
        	name = "unknown";
        people.close();
    	return name;
    }
    
	@Override
	public void onUtteranceCompleted(String utteranceId)
	{
		if (voicerecenabled && !this.phonestatechanged)
		{			
	    	startVoicerec();
		}
		else
			stopSelf();
	}
	
	private void startVoicerec()
	{
		Intent i = new Intent();
    	i.setClass(this, VoiceRecognitionActivity.class);
    	i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     	startActivity(i);
     	stopSelf();   	
	}

	@Override
	public void onInit(int status) 
	{
		mTts.setOnUtteranceCompletedListener(TextToSpeecher.this);
		phonenumber = PhoneStateBroadcastReceiver.getPhoneNumber();
		
		if (status == TextToSpeech.SUCCESS) 
        {			
           int result = mTts.setLanguage(Locale.US);
           if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) 
           {
             // Log.e(TAG, "Language is not available.");
           } 
           else if(!this.phonestatechanged)
           {
	    	    final String contactname = getContactName(phonenumber);
	    	    new DSoundpool(this); 
	    	    Timer timer = new Timer();
	   	    	timer.schedule(new TimerTask()
	   	    	{
	   	    	   public void run()
	   	    	   {	
	   	    		   if (!phonestatechanged)
	   	    			   speechText(contactname);   	    		
	   	    	   }
	   	    	}, 3000);	    	   
           }        	 
        }
		else 
        {             
           //Log.e(TAG, "Could not initialize TextToSpeech.");
        }
	}
	
	private void addTelephoneStatelistner()
	{
		TelephonyManager telephony = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		PhoneStateListener phoneListener = new PhoneStateListener()
		{
			@Override
			public void onCallStateChanged(int state, String incomingNumber) 
			{
				if (state != TelephonyManager.CALL_STATE_RINGING ) 
				{
					phonestatechanged = true;
					stopSelf();
				}
				super.onCallStateChanged(state, incomingNumber);
			}
		};
		telephony.listen(phoneListener,PhoneStateListener.LISTEN_CALL_STATE);
	}
	
	@Override
	public void onDestroy() 
    {		
        if (mTts != null) 
        {
	      mTts.stop();
	      mTts.shutdown();
        }
        super.onDestroy();
    }
	
	@Override
	public IBinder onBind(Intent intent) 
	{	
		return null;
	}
}
