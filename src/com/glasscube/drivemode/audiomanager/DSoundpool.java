package com.glasscube.drivemode.audiomanager;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import com.glasscube.drivemode_beta.R;


public class DSoundpool {
	private static SoundPool soundPool;
	private static  int soundID;
	private static boolean loaded = false;
	private Context context;
	private static  int stream;
	public DSoundpool(Context context)
	{
		this.context = context;
		loadSound();
	}
	public void loadSound()
	{
			
//		if (DAudioManager.headsetplugged)
//    	{
//			stream = AudioManager.STREAM_RING;
//	  	}
//		else
//		{
			stream = AudioManager.STREAM_MUSIC;
		//}
		soundPool = new SoundPool(10, stream, 0);
		soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() 
		{
			@Override
			public void onLoadComplete(SoundPool soundPool, int sampleId,int status) 
			{
				loaded = true;
			}
		});
		soundID = soundPool.load(context, R.raw.beep, 1);
	}
	
	public static void playSound()
	{
		if (loaded) {
			DAudioManager.audiomanager.setStreamVolume(stream,15, 0);
			soundPool.play(soundID, 1, 1, 1, 0, 1f);
		}
	}
	
}
