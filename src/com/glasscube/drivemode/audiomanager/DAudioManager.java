package com.glasscube.drivemode.audiomanager;

import android.content.Context;
import android.media.AudioManager;

public class DAudioManager 
{
	public static int ringvolumelevel;
	public static int musicvolumelevel;
	public static int voicecallvolumelevel;
	public static int voicecallmaxvolumelevel;
	public static boolean headsetplugged;
	public static boolean isheadsethasmic;
	public static boolean ismicmute;
	public static boolean ismusicactive;
	public static AudioManager audiomanager;
	
	public DAudioManager(Context context) 
	{
		audiomanager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		init();
	}
	
	private void init()
	{
		ringvolumelevel = audiomanager.getStreamVolume(AudioManager.STREAM_RING);
		musicvolumelevel = audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC);
		voicecallvolumelevel = audiomanager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		voicecallmaxvolumelevel = audiomanager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
		ismicmute = audiomanager.isMicrophoneMute();
	} 
	
	public static void setDefaultVolumelevels()
	{
		audiomanager.setRingerMode(AudioManager.RINGER_MODE_NORMAL); 
		audiomanager.setStreamVolume(AudioManager.STREAM_RING,ringvolumelevel,0);
	    audiomanager.setStreamVolume(AudioManager.STREAM_MUSIC,musicvolumelevel,0);
	    audiomanager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,voicecallvolumelevel,0);
	}
	
	public static void setTtsVolume(int mlevel,int rlevel)
	{
		audiomanager.setStreamVolume(AudioManager.STREAM_MUSIC,mlevel,0);
		audiomanager.setStreamVolume(AudioManager.STREAM_RING,rlevel,0);
	}
	
	public static void muteAllStreams()
	{
		audiomanager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
	}
}
