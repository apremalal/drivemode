package com.glasscube.drivemode.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.glasscube.drivemode.audiomanager.DAudioManager;
import com.glasscube.drivemode.license.LicenseChecker;
import com.glasscube.drivemode.preferences.Preferences;
import com.glasscube.drivemode_beta.R;

public class MainActivity extends Activity {
	private final int LICENSE_CHECK_CODE = R.string.license_check;
	private final int DATA_CHECK_CODE = R.string.datacheck;
	private final int dialog_tts_data = DATA_CHECK_CODE;
	private Intent installIntent;
	private SharedPreferences preferences;
	private LicenseChecker licener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		licener = new LicenseChecker(MainActivity.this);
		if (licener.isLicensed()) {
			if (DriveModeService.isinstanceCreated()) {
				stopService(new Intent(MainActivity.this,
						DriveModeService.class));
				finish();
			} else {
				new DAudioManager(MainActivity.this);
				setContentView(R.layout.welcome);
				new Dinitializer(MainActivity.this);
				startService(new Intent(MainActivity.this,
						DriveModeService.class));

				preferences = PreferenceManager
						.getDefaultSharedPreferences(MainActivity.this);
				boolean tts_enabled = preferences
						.getBoolean("tts_enable", true);

				if (tts_enabled)
					checkTtsData();
				else
					showWelcomeScreen();
			}
		} else {
			showDialog(R.string.license_check);
		}
	}

	private void checkTtsData() {
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		PackageManager pm = getPackageManager();
		ResolveInfo resolveInfo = pm.resolveActivity(checkIntent,
				PackageManager.MATCH_DEFAULT_ONLY);
		if (resolveInfo != null) {
			startActivityForResult(checkIntent, DATA_CHECK_CODE);
		} else {
			showWelcomeScreen();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DATA_CHECK_CODE
				&& resultCode != TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
			installIntent = new Intent();
			installIntent
					.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
			showDialog(dialog_tts_data);
		} else
			showWelcomeScreen();

		super.onActivityResult(requestCode, resultCode, data);
	}

	private void showWelcomeScreen() {
		final Thread welcomeThread = new Thread() {
			int wait = 0;
			int welcomeScreenDisplay = 3000;

			@Override
			public void run() {
				try {
					super.run();
					while (wait < welcomeScreenDisplay) {
						sleep(100);
						wait += 100;
					}
					finish();
				} catch (Exception e) {
				}

			}
		};
		Button settings = (Button) findViewById(R.id.bsettings);
		settings.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				welcomeThread.interrupt();
				startActivity(new Intent(MainActivity.this, Preferences.class));
				finish();
			}
		});
		welcomeThread.start();
	}

	protected Dialog onCreateDialog(int id) {

		switch (id) {

		case dialog_tts_data:
			return new AlertDialog.Builder(this)
					.setTitle("Speech Engine Data")
					.setMessage(
							"This application requires TTS to announce who is calling.Would you like to download it now?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									startActivity(installIntent);
									finish();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									SharedPreferences.Editor editor = preferences
											.edit();
									editor.putBoolean("tts_enable", false);
									editor.commit();
									finish();
								}
							}).create();

		case LICENSE_CHECK_CODE:
			return new AlertDialog.Builder(this)
					.setTitle(R.string.licensed_expire_dialog_title)
					.setMessage(R.string.unlicensed_dialog_body)
					.setPositiveButton(R.string.buy_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									Intent marketIntent = new Intent(
											Intent.ACTION_VIEW,
											Uri.parse("https://play.google.com/store/apps/details?id=com.glasscube.drivemodepro"));
									startActivity(marketIntent);
									finish();

								}
							})
					.setNegativeButton(R.string.quit_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							}).create();
		}

		return super.onCreateDialog(id);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
