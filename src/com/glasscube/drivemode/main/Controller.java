package com.glasscube.drivemode.main;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.glasscube.drivemode.preferences.Preferences;
import com.glasscube.drivemode_beta.R;

public class Controller extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.controller);
			Button bsettings = (Button) findViewById(R.id.bsettings_controller);
			bsettings.setOnClickListener(new OnClickListener() 
			{
				public void onClick(View v)
				{
					startActivity(new Intent(Controller.this,Preferences.class));
					finish();
				}
			});
			
			final Button tbutton = (Button) findViewById(R.id.buttonoff);
			
			tbutton.setOnClickListener(new OnClickListener() 
			{
				public void onClick(View v)
				{
					if(DriveModeService.isinstanceCreated())
			 		{
			         	stopService(new Intent(Controller.this, DriveModeService.class));
			         	finish();          
			 		}
				}
			});
	}

}
