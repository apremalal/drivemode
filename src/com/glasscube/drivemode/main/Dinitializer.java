package com.glasscube.drivemode.main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Environment;
import android.widget.Toast;


public class Dinitializer
{
	private Context context;
	private String extStorageDirectory;
	
	public Dinitializer(Context context) 
	{
		this.context = context;
		firstRun();
	}
	
	public void firstRun()
	{		
	    SharedPreferences settings = context.getSharedPreferences("DriveMode", 0);
        boolean firstrun = settings.getBoolean("firstrun", true);
        
        if (firstrun) // Checks to see if we've ran the application before
        { 
        	extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            SharedPreferences.Editor e = settings.edit();
            e.putBoolean("firstrun", false);
            e.commit();
            setDirectory();// If not, run these methods
        } 
	}
		
	private void setDirectory()
	{
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) 
		{
			File txtDirectory = new File(extStorageDirectory + "/drivemode/com.glasscube.drivemode.voicerecognition/hmm/en_US/hub4wsj_sc_8k");
            txtDirectory.mkdirs();
            txtDirectory = new File(extStorageDirectory + "/drivemode/com.glasscube.drivemode.voicerecognition/lm/en_US");
            txtDirectory.mkdirs();
            copyAssets(); 
        } else if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED_READ_ONLY))
        {
        	Toast.makeText(context, "SDcard missing",Toast.LENGTH_SHORT).show();
        }
	}

	private void copyAssets()
	{
		AssetManager assetManager = context.getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {  }
        
        for (int i = 0; i < files.length; i++) 
        {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(files[i]);
                if(files[i].equals("feat.params")||files[i].equals("mdef")||files[i].equals("means")||files[i].equals("noisedict")||files[i].equals("sendump")||files[i].equals("transition_matrices")||files[i].equals("variances"))
                	out = new FileOutputStream(extStorageDirectory + "/drivemode/com.glasscube.drivemode.voicerecognition/hmm/en_US/hub4wsj_sc_8k/" + files[i]);
                else if(files[i].equals("hub4.5000.DMP")||files[i].equals("wsj0vp.5000.DMP")||files[i].equals("hub4.5000.dic"))
                	out = new FileOutputStream(extStorageDirectory + "/drivemode/com.glasscube.drivemode.voicerecognition/lm/en_US/" + files[i]);
                else
                	continue;
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {  }
        }
	}
	
	
	private void copyFile(InputStream in, OutputStream out)throws IOException
	{
		byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0, read);
        }		
	}

}
