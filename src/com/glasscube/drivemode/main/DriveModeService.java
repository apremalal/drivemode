package com.glasscube.drivemode.main;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

import com.glasscube.drivemode_beta.R;
import com.glasscube.drivemode.broadcasthandler.HeadsetStateBroadCastReceiver;
import com.glasscube.drivemode.broadcasthandler.PhoneStateBroadcastReceiver;
import com.glasscube.drivemode.respoder.SmsResponder;

public class DriveModeService extends Service 
{
	private PhoneStateBroadcastReceiver ps_br;
	private HeadsetStateBroadCastReceiver hs_br;
	
	private IntentFilter pfilter;
	private IntentFilter hfilter;
	
	private Notification note;
	private final IBinder mBinder = new MyBinder();
	private static boolean isrunning = false;

	@Override
	public void onCreate() 
	{
	  super.onCreate();	
	  hs_br = new HeadsetStateBroadCastReceiver();
	  ps_br = new PhoneStateBroadcastReceiver();
	  
	  pfilter = new IntentFilter();
	  hfilter = new IntentFilter();

	  hfilter.addAction("android.intent.action.HEADSET_PLUG");
	  pfilter.addAction("android.intent.action.PHONE_STATE");
	
	  
	  Intent hs_intent = registerReceiver(hs_br, hfilter);
	  
	  if (hs_intent != null)
	  {
		  hs_br.onReceive(DriveModeService.this,hs_intent);
	  }
	  
	  registerReceiver(ps_br, pfilter); 
	  this.isrunning = true;
	  showNotification();
	}
	
	private void showNotification()
	{       
		note = new Notification(R.drawable.ic_notification, "Drive MODE", System.currentTimeMillis());
		Intent intent = new Intent(this,Controller.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pendingintent = PendingIntent.getActivity(this, 0,intent, 0);
		note.setLatestEventInfo(this, "Drive MODE","Your calls are taken care of! :)",pendingintent);
		note.flags |= Notification.FLAG_NO_CLEAR;
		startForeground(R.string.app_name, note);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0)
	{
		return mBinder;
	}

	public class MyBinder extends Binder 
	{
		DriveModeService getService() 
		{
			return DriveModeService.this;
		}
	}
	
    public static boolean isinstanceCreated()
    {
    	return isrunning;
    }
    
	@Override
	public void onDestroy() 
	{
		unregisterReceiver(ps_br);
		unregisterReceiver(hs_br);
		
		if(SmsResponder.soundPool != null)
			SmsResponder.soundPool.unload(SmsResponder.soundID_smssent);
		
		this.isrunning = false;
		Toast.makeText(DriveModeService.this, "Drive MODE disabled",Toast.LENGTH_SHORT).show();
		stopForeground(true);
		super.onDestroy();
	}
}
