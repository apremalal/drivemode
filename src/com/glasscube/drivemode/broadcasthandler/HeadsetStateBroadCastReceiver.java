package com.glasscube.drivemode.broadcasthandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.glasscube.drivemode.audiomanager.DAudioManager;

public class HeadsetStateBroadCastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle extras = intent.getExtras();
		if (extras != null)
		{
			int state = intent.getIntExtra("state" , -1); //0 for unplugged, 1 for plugged.
			int mic = intent.getIntExtra("microphone", -1); //1 if headset has a microphone, 0 otherwise
			if(state == 1 && mic == 1) //headset pluged
			{
				DAudioManager.headsetplugged = true;
				DAudioManager.isheadsethasmic = true;
				Toast.makeText(context, "Headset Plugged",Toast.LENGTH_SHORT).show();
			}
			else if (state == 1 && mic == 0)// headset pluged no microphone
			{
				DAudioManager.headsetplugged = true;
				DAudioManager.isheadsethasmic = false;
				Toast.makeText(context, "Headset Plugged",Toast.LENGTH_SHORT).show();
			}
			else if(state == 0)
			{
				DAudioManager.headsetplugged = false;
				DAudioManager.isheadsethasmic = false;
			}
		}
	}
}
