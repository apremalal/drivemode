
package com.glasscube.drivemode.broadcasthandler;

import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import com.glasscube.drivemode.audiomanager.DAudioManager;
import com.glasscube.drivemode.respoder.SmsResponder;
import com.glasscube.drivemode.tts.TextToSpeecher;

public class PhoneStateBroadcastReceiver extends BroadcastReceiver 
{
	private static String phonenumber;
	public static String state;
	AudioManager audiomanager;
	private boolean answered = false;
	
	@Override	
	public void onReceive(final Context context, Intent intent) 
	{  		
		Bundle extras = intent.getExtras();
		audiomanager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		if (extras != null)
		{
			state = extras.getString(TelephonyManager.EXTRA_STATE);
			if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)&& !this.answered) 
			{
				new DAudioManager(context);	
				phonenumber = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
	        	final Intent intent2open = new Intent(context, TextToSpeecher.class);
	    	    intent2open.putExtra("PHONE_NUMBER",phonenumber );
	    	    intent2open.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	  	    	context.startService(intent2open);
			}
			else if(state.equals(TelephonyManager.EXTRA_STATE_IDLE)) //cut the phone
			{				
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
				boolean rejectionsms = preferences.getBoolean("rejection_method", true);
				
				if (!SmsResponder.actiontaken && rejectionsms && !this.answered)
				{
					String sms = preferences.getString("sms","Hey! Sorry! Can't pick up the phone. I'm driving right now.via Drive MODE");
					new SmsResponder(context).sendSms(sms,phonenumber);
					
				}
				if (!audiomanager.isBluetoothScoOn() && !audiomanager.isBluetoothA2dpOn())	
				{
					DAudioManager.setDefaultVolumelevels();
				}
				SmsResponder.actiontaken = false;
				this.answered = false;
				phonenumber = "0dmode";
			}			
			else if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) // answer call || take a call
			{
				phonenumber = "0dmode";
				this.answered = true;
			    Timer timer = new Timer();
	   	    	timer.schedule(new TimerTask()
	   	    	{
	   	    	   public void run()
	   	    	   {	   	    		   
	   	    		if (!audiomanager.isBluetoothScoOn() && !audiomanager.isBluetoothA2dpOn() && !DAudioManager.headsetplugged)
	 				{
				    	audiomanager.setSpeakerphoneOn(true);
				    	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
						int volume = Integer.parseInt(preferences.getString("speakerphone_volume",""+DAudioManager.voicecallmaxvolumelevel));
						audiomanager.setStreamVolume(AudioManager.STREAM_VOICE_CALL,volume, 0);
	 				}		    
	   	    	   }
	   	    	}, 1000);
	   		}
		}
    }
	public static String getPhoneNumber()
	{
		return phonenumber;
	}
}
