package com.glasscube.drivemode.preferences;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.speech.tts.TextToSpeech;
import android.widget.Toast;

import com.glasscube.drivemode_beta.R;
import com.glasscube.drivemode.audiomanager.DAudioManager;

public class Preferences extends PreferenceActivity {

	private final int DATA_CHECK_CODE = R.string.datacheck;
	private Intent installIntent;
	private CheckBoxPreference chbEnableTts;
	private CheckBoxPreference chbLimitSMS;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		Preference button_done = (Preference) findPreference("button_done");
		Preference button_help = (Preference) findPreference("button_help");

		ListPreference speakervolume = (ListPreference) findPreference("speakerphone_volume");
		chbEnableTts = (CheckBoxPreference) findPreference("tts_enable");
		chbLimitSMS = (CheckBoxPreference) findPreference("non_contact_rejection");
		
		int speaker_max_volume = DAudioManager.voicecallmaxvolumelevel;
		String H = Integer.toString(speaker_max_volume), M = Integer
				.toString(speaker_max_volume / 2), L = "1";
		CharSequence c[] = { H, M, L };
		speakervolume.setEntryValues(c);
		speakervolume.setDefaultValue(H);

		button_done
				.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
					@Override
					public boolean onPreferenceClick(Preference arg0) {
						finish();
						return true;
					}
				});
		button_help
				.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
					@Override
					public boolean onPreferenceClick(Preference arg0) {
						Intent i = new Intent();
						i.setClass(Preferences.this, Help.class);
						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);
						return true;
					}
				});
		
		chbEnableTts
				.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						if (newValue.toString().equalsIgnoreCase("true")) {
							checkTtsData();
						}

						return true;
					}
				});

	}

	private void checkTtsData() {
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		PackageManager pm = getPackageManager();
		ResolveInfo resolveInfo = pm.resolveActivity(checkIntent,
				PackageManager.MATCH_DEFAULT_ONLY);
		if (resolveInfo != null) {
			startActivityForResult(checkIntent, DATA_CHECK_CODE);
		} else {
			Toast.makeText(Preferences.this, "Unable to check TTS Data",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DATA_CHECK_CODE
				&& resultCode != TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
			installIntent = new Intent();
			installIntent
					.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
			showDialog(DATA_CHECK_CODE);
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {

		case DATA_CHECK_CODE:
			return new AlertDialog.Builder(this)
					.setTitle("Speech Engine Data")
					.setMessage(
							"This application requires TTS to announce who is calling.Would you like to download it now?")
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									startActivity(installIntent);
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									chbEnableTts.setChecked(false);
								}
							}).create();
		}

		return super.onCreateDialog(id);
	}
}
