package com.glasscube.drivemode.preferences;

import com.glasscube.drivemode_beta.R;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class Help extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
        TextView tv = (TextView) findViewById(R.id.textView9);
        tv.setText(Html.fromHtml("For more details visit: <a href=\"http://www.glasscubehub.com/aboutourproducts/drivemode\">Drive MODE</a> "));
        tv.setMovementMethod(LinkMovementMethod.getInstance());
	}
}
