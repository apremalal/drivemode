package com.glasscube.drivemode.respoder;


import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.widget.Toast;

import com.glasscube.drivemode_beta.R;
import com.glasscube.drivemode.audiomanager.DAudioManager;

public class SmsResponder
{
	String SENT = "SMS_SENT",DELIVERED = "SMS_DELIVERED";
	public static boolean actiontaken = false;
	static Context context;
 
    static BroadcastReceiver br = null ;
    public static SoundPool soundPool;
    static boolean loaded;
    public static int soundID_smssent;
    
	public SmsResponder(Context context)
	{
		this.context = context;
		 soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
			soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() 
			{
				@Override
				public void onLoadComplete(SoundPool soundPool, int sampleId,int status) 
				{
					loaded = true;
				}
			});
			if(!loaded)
				soundID_smssent = soundPool.load(context , R.raw.smssent, 1);		
	}
	
	public void sendSms(String message,String phonenumber)
	{		
		 br = new BroadcastReceiver()
		 {
            @Override
            public void onReceive(Context context, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                    	
                    	DAudioManager.audiomanager.setStreamVolume(AudioManager.STREAM_MUSIC,15, 0);
                    	Toast.makeText(context, "Call rejection SMS sent",Toast.LENGTH_SHORT).show();
                       	Timer timer = new Timer();
        	   	    	timer.schedule(new TimerTask()
        	   	    	{
        	   	    	   public void run()
        	   	    	   {	   	    		   
        	   	    		if(loaded)
                    		{
                      			soundPool.play(soundID_smssent, 15, 15, 1, 0, 1f);
                      			soundPool.unload(soundID_smssent);
                      		}
        	   	    	   }
        	   	    	},1000);                 
                        break;
                    
                }
            }
        };	
        context.registerReceiver(br, new IntentFilter(SENT));
       	PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,new Intent(SENT), 0);
		PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,new Intent(DELIVERED), 0);
		CompatibilitySmsManager.getDefault().sendTextMessage(phonenumber, null, message, sentPI,deliveredPI);		
	}
	
	public static void unregisterBroadCastReceiver()
	{
		if (br != null)
		{
			context.unregisterReceiver(br);
			br = null;
		}	
	}
}
