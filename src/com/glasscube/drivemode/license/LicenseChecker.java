package com.glasscube.drivemode.license;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;

 public class LicenseChecker 
{
	private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private final long ONE_DAY = 24 * 60 * 60 * 1000;
	private boolean licened = false;
	private Context context;
	public LicenseChecker(Context context) 
	{
		this.context = context;
	}
	
   public  boolean isLicensed() 
   {
	    SharedPreferences preferences = context.getSharedPreferences("InstalledDate",context.MODE_PRIVATE);
	    String installDate = preferences.getString("InstallDate", null);
	    if(installDate == null) 
	    {
	        SharedPreferences.Editor editor = preferences.edit();
	        Date now = new Date();
	        String dateString = formatter.format(now);
	        editor.putString("InstallDate", dateString);
	        editor.commit();
	        licened = true;
	    }
	    else
	    {
	        Date before = null;
			try {
				before = (Date)formatter.parse(installDate);
				Date now = new Date();
		        long diff = now.getTime() - before.getTime();
		        long days = diff / ONE_DAY;
		        if(days > 30) 
		        { 
		        	licened = false;	            
		        }else 
		        	licened = true;
		        
			} catch (ParseException e) {}
	    }
	        
		return licened;
   }
 }

