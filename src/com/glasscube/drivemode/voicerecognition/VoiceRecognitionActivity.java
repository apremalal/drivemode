package com.glasscube.drivemode.voicerecognition;

import java.lang.reflect.Method;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.internal.telephony.ITelephony;
import com.glasscube.drivemode_beta.R;
import com.glasscube.drivemode.audiomanager.DAudioManager;
import com.glasscube.drivemode.audiomanager.DSoundpool;
import com.glasscube.drivemode.broadcasthandler.PhoneStateBroadcastReceiver;
import com.glasscube.drivemode.respoder.SmsResponder;

public class VoiceRecognitionActivity extends Activity implements RecognitionListener 
{
	private  boolean action_taken = false, action_no = false ;
	private ITelephony telephonyService;
	public static long start_time;
	AudioManager audiomanager;
	String results[];
	private  CountDownTimer cdt;
	
	static {System.loadLibrary("pocketsphinx_jni");	}
	
	/**
	 * Recognizer task, which runs in a worker thread.
	 */	
	
	RecognizerTask rec;
	/**
	 * Thread in which the recognizer task runs.
	 */
	Thread rec_thread;
	/**
	 * Time at which current recognition started.
	 */
	public static Date start_date;
	/**
	 * Number of seconds of speech.
	 */
	float speech_dur;
	/**
	 * Are we listening?
	 */
	boolean listening;
	/**
	 * Progress dialog for final recognition.
	 */
	ProgressDialog rec_dialog;
	/**
	 * Performance counter view.
	 */
	TextView performance_text;
	/**
	 * Editable text view.
	 */
	EditText edit_text;
	private boolean sendsms;
	private SharedPreferences preferences;
	private SmsResponder smsresponder;
	public void onCreate(Bundle savedInstanceState) 
	{
		
		super.onCreate(savedInstanceState);
		addTelephoneStatelistner();
		getWindow().addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED |LayoutParams.FLAG_DISMISS_KEYGUARD | LayoutParams.FLAG_TURN_SCREEN_ON | LayoutParams.FLAG_KEEP_SCREEN_ON);		
		setContentView(R.layout.recognition); 
		final boolean flistning = this.listening;
	    preferences = PreferenceManager.getDefaultSharedPreferences(this);
		sendsms = preferences.getBoolean("rejection_method", true);
		
		if(sendsms)
			smsresponder = new SmsResponder(VoiceRecognitionActivity.this);
		
		this.action_taken = false;
		this.rec = new RecognizerTask();
		this.rec_thread = new Thread(this.rec);
		
		audiomanager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
	
		muteAllStreams();
		
		if (DAudioManager.ismicmute)
			DAudioManager.audiomanager.setMicrophoneMute(false);
						
		this.listening = true;
		this.rec.setRecognitionListener(this);
		
		DSoundpool.playSound();
				
		this.rec_thread.start();
		this.rec.start();
		
		showProgress();   	
	}
	
	private void muteAllStreams()
	{
		audiomanager.setRingerMode(AudioManager.RINGER_MODE_SILENT); // if set to zero phone will vibrate
		audiomanager.setStreamVolume(AudioManager.STREAM_MUSIC,0,0);
	}

	private void shutDownRecognizer()
	{
		this.rec.shutdown();
	}
	
	private void showProgress()
	{
		int total = 8000;
		final ProgressBar bar = (ProgressBar) findViewById(R.id.progressBar);
	    bar.setProgress(total);
	    bar.setMax(total);     
	    cdt = new CountDownTimer(total, 1) 
	    { 
	        public void onTick(long millisUntilFinished) 
	        {	     	
	       
	        	if(listening)
	        	{        		
	        		bar.setProgress((int)millisUntilFinished);
	        	} 
	        }
	        
			@Override
			public void onFinish()
			{
				if(listening)
	        	{
					setContentView(R.layout.blank);
	           		DSoundpool.playSound();
		            stopRecognizer();
	        	}				
			}
	    }.start();	    
	}
	
	private void stopRecognizer()
    {
    	this.rec.stop();
    	this.listening =  false;
    }
	
	private void getAction(String lastcommand)
    {
    	if(this.listening)
    		stopRecognizer();
    	
    	this.action_taken = true;
    	
    	if(lastcommand.equals("yes"))
		{    		
    		TelephonyManager telephony = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
    		
    		try 
    		{
    			Class c = Class.forName(telephony.getClass().getName());
    			Method m = c.getDeclaredMethod("getITelephony");
    			m.setAccessible(true);
    			telephonyService = (ITelephony) m.invoke(telephony);
    			telephonyService.silenceRinger();
    			telephonyService.answerRingingCall();
    		} catch (Exception e) 
    		{
    			Intent answer = new Intent(Intent.ACTION_MEDIA_BUTTON);
    			answer.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
    			sendOrderedBroadcast(answer, null);
//    			    		 
//    			Intent answer2 = new Intent(Intent.ACTION_MEDIA_BUTTON);
//    			answer2.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
//    			sendOrderedBroadcast(answer2, null);
    			//works with thale under
    		}						
			finish();
	    }
		else if(lastcommand.equals("no"))
		{			
			if (sendsms)
			{
				SmsResponder.actiontaken = true;
				this.action_no = true;
				String sms = preferences.getString("sms","Hey! Sorry! Can't pick up the phone. I'm driving right now.via Drive MODE");
				smsresponder.sendSms(sms,PhoneStateBroadcastReceiver.getPhoneNumber());
			}
			finish();
		}
    }
		
	public void onPartialResults(Bundle b) 
	{
		
		final String hyp = b.getString("hyp");
		String temp = hyp;
		
		String lastcommand = null;		
		if ((temp!=null && temp.length()>= 2) && !this.action_taken)
		{	
		   this.action_taken = true;
		   results = temp.split("\\s+");
		   lastcommand=results[results.length-1];
		   if(cdt != null)
		   {
			   cdt.cancel();
			   cdt = null;
		   }
		   stopRecognizer();
		   getAction(lastcommand);
		}
	}
	
	private void addTelephoneStatelistner()
	{
		TelephonyManager telephony = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		PhoneStateListener phoneListener = new PhoneStateListener()
		{
			@Override
			public void onCallStateChanged(int state, String incomingNumber) 
			{
				if (state != TelephonyManager.CALL_STATE_RINGING ) 
				{
					if(listening)
					{
						stopRecognizer();
						shutDownRecognizer();
					}
					finish();
				}
				super.onCallStateChanged(state, incomingNumber);
			}
		};
		telephony.listen(phoneListener,PhoneStateListener.LISTEN_CALL_STATE);
	}
	
	public void onResults(Bundle b)
	{
		final String hyp = b.getString("hyp");
		String temp = hyp;
		String lastcommand = null;
		if(cdt != null)
	    {
		   cdt.cancel();
		   cdt = null;
	    }
		if (temp!=null && temp.length()>= 2 && !this.action_taken)
		{
		   results = temp.split("\\s+");
		   lastcommand=results[results.length-1];	
		   getAction(lastcommand);
		}
		else if(!this.action_taken)
		{
			DAudioManager.setDefaultVolumelevels();
			finish();
		}
		else
			finish();
	}
	
	public void onError(int err){}
	
	@Override
	protected void onDestroy() 
	{	
		if(this.listening)
		{
			stopRecognizer();
			shutDownRecognizer();
		}
	
		SmsResponder.actiontaken = true;
		if (action_no)
		{							
			SmsResponder.unregisterBroadCastReceiver();
			action_no = false;
		}
		super.onDestroy();
	}
}